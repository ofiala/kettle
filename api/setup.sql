CREATE TYPE side AS ENUM ('alpha', 'bravo');
CREATE TYPE sex AS ENUM ('male', 'female');
CREATE TYPE species AS ENUM ('inkling', 'octoling');

CREATE TABLE users (
		name VARCHAR(10) NOT NULL UNIQUE,
		side side NOT NULL,
		api_key CHAR(16) PRIMARY KEY
);

CREATE TABLE matches (
	id BIGSERIAL PRIMARY KEY,
	stage_id SMALLINT NOT NULL,
	winning_side side NOT NULL,
	alpha_painted_percent REAL NOT NULL,
	bravo_painted_percent REAL NOT NULL,
	start_timestamp INTEGER NOT NULL,
	end_timestamp INTEGER NOT NULL,
	created_timestamp INTEGER NOT NULL DEFAULT EXTRACT(EPOCH FROM CURRENT_TIMESTAMP(0) AT TIME ZONE 'GMT'),
	splatnet_match_id INTEGER NOT NULL,
	submitters_key CHAR(16) NOT NULL REFERENCES users(api_key),
	CONSTRAINT matches_no_duplicates UNIQUE (submitters_key, splatnet_match_id)
);

CREATE TABLE players (
	match_id BIGINT NOT NULL REFERENCES matches(id),
	side side NOT NULL,
	name VARCHAR(10) NOT NULL,
	splatnet_player_id CHAR(16) NOT NULL,
	is_submitter BOOLEAN NOT NULL,
	is_disconnect BOOLEAN NOT NULL,
	sex sex NOT NULL,
	species species NOT NULL,
	weapon_id SMALLINT NOT NULL,
	level SMALLINT NOT NULL,
	points SMALLINT NOT NULL,
	kill_count SMALLINT NOT NULL,
	assist_count SMALLINT NOT NULL,
	death_count SMALLINT NOT NULL,
	special_count SMALLINT NOT NULL,
	PRIMARY KEY (match_id, splatnet_player_id)
);


/***********************************************************************
 * Modify the command below to create a new user                       *
 * =============================================                       *
 *                                                                     *
 * IMPORTANT! Username must be unique and its length may not exceed 10 *
 * characters. API key must be unique, exactly 16 characters long, and *
 * should be generated using a cryptographically secure random number  *
 * generation algorithm to prevent it from being guessed. You can use  *
 * the following command to generate the API key using your system's   *
 * random number generator:                                            *
 * $ head -c 160 /dev/urandom | tr -dc [:alnum:] | head -c 16 ; echo   *
 ***********************************************************************/

INSERT INTO users (
	name,
	side,
	api_key
) VALUES (
	'USERNAME',
	'SIDE (alpha/bravo)',
	'API KEY'
);
