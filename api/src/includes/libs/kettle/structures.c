#include <kore/kore.h>
#include "structures.h"

/*!
 * @brief Initializes the given match struct's players to NULL.
 */
void match_init_players(struct match* match){
	if(match == NULL)
		return;

	for(int i = 0;i < 4;i++){
		match->alpha.players[i] = NULL;
	}

	for(int i = 0;i < 4;i++){
		match->bravo.players[i] = NULL;
	}

	match->submitter = NULL;
}

/*!
 * @brief Frees the match struct's players and sets them to NULL. They must be either alloced with kore_malloc or set to NULL for this to work.
 */
void match_free_players(struct match* match){
	if(match == NULL)
		return;

	for(int i = 0;i < 4;i++){
		kore_free(match->alpha.players[i]);
		match->alpha.players[i] = NULL;
	}
	match->alpha.player_count = 0;

	for(int i = 0;i < 4;i++){
		kore_free(match->bravo.players[i]);
		match->bravo.players[i] = NULL;
	}
	match->bravo.player_count = 0;

	//match->submitter should point to one of the players,
	//therefore we don't free it here
	match->submitter = NULL;
}
