#ifndef KETTLE_STRUCTURES_H
#define KETTLE_STRUCTURES_H

#include <stdbool.h>

enum side{alpha, bravo};

enum sex{male, female};

enum species{inkling, octoling};

struct user {
	enum side side;
	char name[41];
	char api_key[17];
};

struct player {
	enum sex sex;
	enum species species;
	bool is_submitter;
	bool is_disconnect;
	char splatnet_player_id[17];
	char name[41];
	short weapon_id;
	short level;
	short points;
	short kill_count;
	short assist_count;
	short death_count;
	short special_count;
};

struct team {
	short player_count;
	short disconnect_count;
	float painted_percent;
	struct player* players[4];
};

struct match {
	enum side winning_side;
	short stage_id;
	long splatnet_match_id;
	long start_timestamp;
	long end_timestamp;
	struct team alpha;
	struct team bravo;
	struct player* submitter;
};

void match_init_players(struct match*);
void match_free_players(struct match*);

#endif
