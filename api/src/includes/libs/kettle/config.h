#ifndef KETTLE_CONFIG_H
#define KETTLE_CONFIG_H

struct config {
	char db_host[256];
	char db_name[256];
	char db_user[256];
	char db_password[256];
	char alpha_prefix[41];
	char bravo_prefix[41];
	int min_team_player_count;
};

extern struct config g_config;

#endif
