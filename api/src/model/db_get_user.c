#include <string.h>
#include <kore/kore.h>
#include <kore/pgsql.h>
#include <kettle/structures.h>
#include <kettle/error.h>

/*!
 * @brief Tries to find a user with the provided api key. The result should not be freed.
 * @param api_key API key of the user
 * @param out Where the user struct is stored
 * @return KETTLE_SUCCESS if the user was found, KETTLE_CLIENT_ERROR if not found, and KETTLE_SERVER_ERROR on error
 */
int db_get_user(const char* api_key, struct user* out){
	if(api_key == NULL || out == NULL){
		kore_log(LOG_ERR, "db_get_user: expected valid pointer, got NULL");
		return KETTLE_SERVER_ERROR;
	}

	struct user* user = out;

	struct kore_pgsql pgsql;
	kore_pgsql_init(&pgsql);

	if(!kore_pgsql_setup(&pgsql, "db", KORE_PGSQL_SYNC)){
		kore_pgsql_logerror(&pgsql);
		goto error;
	}

	if(!kore_pgsql_query_params(&pgsql, "SELECT name, side FROM users WHERE api_key = $1;", 0, 1, KORE_PGSQL_PARAM_TEXT(api_key))){
		kore_pgsql_logerror(&pgsql);
		goto error;
	}

	if(kore_pgsql_ntuples(&pgsql) == 0){
		kore_pgsql_cleanup(&pgsql);
		return KETTLE_CLIENT_ERROR;
	}


	if(kore_pgsql_getlength(&pgsql, 0, 0) > 40){
		kore_log(LOG_ERR, "Database data do not match the expected length!");
		goto error;
	}
	strcpy(user->name, kore_pgsql_getvalue(&pgsql, 0, 0));

	const char* side = kore_pgsql_getvalue(&pgsql, 0, 1);
	if(strcmp(side, "alpha") == 0)
		user->side = alpha;
	else if(strcmp(side, "bravo") == 0)
		user->side = bravo;
	else
		goto error;

	if(strlen(api_key) > 16){
		//while api_key is not supplied by the db here, query should return 0 rows if api_key length isn't 16,
		//so if it returns rows when api_key length > 16, it's broken
		kore_log(LOG_ERR, "Database data do not match the expected length!");

		error:
			kore_pgsql_cleanup(&pgsql);
			return KETTLE_SERVER_ERROR;
	}
	strcpy(user->api_key, api_key);

	kore_pgsql_cleanup(&pgsql);
	return KETTLE_SUCCESS;
}
