#include <kore/kore.h>
#include <kore/pgsql.h>
#include <kettle/structures.h>
#include <kettle/error.h>

#define INT32_MAX 2147483647

char* inttostr(long long num);

/*!
 * @brief Finds all matches uploaded by the given user and returns their splatnet_match_ids as an array of longs. The result should be freed with kore_free if successful.
 * @param submitter Submitter of the matches we're looking for
 * @param limit Maximal count of matches to find; ordered by id descending
 * @param out Where pointer to the result should be stored
 * @param out_count Where the number of members in result should copied into
 * @return KETTLE_SUCCESS on success, KETTLE_SERVER_ERROR on error
 */
int db_get_splatnet_match_ids_by_submitter(struct user submitter, long limit, long** out, long* out_count){
	if(out == NULL || out_count == NULL){
		kore_log(LOG_ERR, "db_get_splatnet_match_ids_by_submitter: expected valid pointer, got NULL");
		return KETTLE_SERVER_ERROR;
	}

	struct kore_pgsql pgsql;

	kore_pgsql_init(&pgsql);

	if(!kore_pgsql_setup(&pgsql, "db", KORE_PGSQL_SYNC)){
		kore_pgsql_logerror(&pgsql);
		goto error;
	}

	if(limit != 0){
		char* limitstr = inttostr(limit);
		if(!kore_pgsql_query_params(&pgsql, "SELECT splatnet_match_id FROM matches WHERE submitters_key = $1 ORDER BY id DESC LIMIT $2;", 0, 2, KORE_PGSQL_PARAM_TEXT(submitter.api_key), KORE_PGSQL_PARAM_TEXT(limitstr))){
			kore_free(limitstr);
			kore_pgsql_logerror(&pgsql);
			goto error;
		}
		kore_free(limitstr);
	}else{
		if(!kore_pgsql_query_params(&pgsql, "SELECT splatnet_match_id FROM matches WHERE submitters_key = $1 ORDER BY id DESC;", 0, 1, KORE_PGSQL_PARAM_TEXT(submitter.api_key))){
			kore_pgsql_logerror(&pgsql);
			goto error;
		}
	}

	long num_rows = kore_pgsql_ntuples(&pgsql);
	long* splatnet_match_ids = kore_malloc(num_rows * sizeof(long));

	for(long long i = 0;i < num_rows;i++){
		int conversionerrno = 0;
		splatnet_match_ids[i] = kore_strtonum(kore_pgsql_getvalue(&pgsql, i, 0), 10, 0, INT32_MAX, &conversionerrno);

		if(conversionerrno != KORE_RESULT_OK){
			kore_log(LOG_ERR, "error converting %s to int32", kore_pgsql_getvalue(&pgsql, i, 0));
			kore_free(splatnet_match_ids);
			goto error;
		}
	}

	*out = splatnet_match_ids;
	*out_count = num_rows;
	kore_pgsql_cleanup(&pgsql);
	return KETTLE_SUCCESS;

	error:
		kore_pgsql_cleanup(&pgsql);
		return KETTLE_SERVER_ERROR;
}
