#include <string.h>
#include <kore/kore.h>
#include <kore/pgsql.h>
#include <kettle/structures.h>
#include <kettle/error.h>

char* inttostr(long long num);

char* floattostr(long double num);

const char* sidetostr(enum side);

const char* sextostr(enum sex);

const char* speciestostr(enum species);

void kore_massfree(int count, ...);

/*!
 * @brief Save the given match, uploaded by the given submitter.
 * @return KETTLE_SUCCESS on success, KETTLE_CLIENT_ERROR on duplicate, and KETTLE_SERVER_ERROR on error
 */
int db_save_match_data(struct match match, struct user submitter){
	struct kore_pgsql pgsql;

	kore_pgsql_init(&pgsql);

	if(!kore_pgsql_setup(&pgsql, "db", KORE_PGSQL_SYNC)){
		kore_pgsql_logerror(&pgsql);
		return KETTLE_SERVER_ERROR;
	}

	const char* query = "INSERT INTO matches ( \
				stage_id, \
				winning_side, \
				alpha_painted_percent, \
				bravo_painted_percent, \
				start_timestamp, \
				end_timestamp, \
				splatnet_match_id, \
				submitters_key \
			) VALUES ($1, $2, $3, $4, $5, $6, $7, $8) \
			RETURNING id;";

	char* stage_id = inttostr(match.stage_id);
	char* alpha_painted_percent = floattostr(match.alpha.painted_percent);
	char* bravo_painted_percent = floattostr(match.bravo.painted_percent);
	char* start_timestamp = inttostr(match.start_timestamp);
	char* end_timestamp = inttostr(match.end_timestamp);
	char* splatnet_match_id = inttostr(match.splatnet_match_id);

	if(!kore_pgsql_query_params(&pgsql, query, 0, 8,
		KORE_PGSQL_PARAM_TEXT(stage_id),
		KORE_PGSQL_PARAM_TEXT(sidetostr(match.winning_side)),
		KORE_PGSQL_PARAM_TEXT(alpha_painted_percent),
		KORE_PGSQL_PARAM_TEXT(bravo_painted_percent),
		KORE_PGSQL_PARAM_TEXT(start_timestamp),
		KORE_PGSQL_PARAM_TEXT(end_timestamp),
		KORE_PGSQL_PARAM_TEXT(splatnet_match_id),
		KORE_PGSQL_PARAM_TEXT(submitter.api_key)
	)){
		kore_massfree(6, stage_id, alpha_painted_percent, bravo_painted_percent, start_timestamp, end_timestamp, splatnet_match_id);

		char* pgsql_already_uploaded = "ERROR:  duplicate key value violates unique constraint \"matches_no_duplicates\"";

		if(strncmp(pgsql.error, pgsql_already_uploaded, strlen(pgsql_already_uploaded)) == 0){
			kore_pgsql_cleanup(&pgsql);
			return KETTLE_CLIENT_ERROR;
		}else{
			kore_pgsql_logerror(&pgsql);
			kore_pgsql_cleanup(&pgsql);
			return KETTLE_SERVER_ERROR;
		}
	}

	kore_massfree(6, stage_id, alpha_painted_percent, bravo_painted_percent, start_timestamp, end_timestamp, splatnet_match_id);

	char* match_id = kore_pgsql_getvalue(&pgsql, 0, 0);

	struct team* team = &match.alpha;
	char* side = "alpha";

	for(int i = 0;i < 2;i++){
		for(int n = 0;n < team->player_count;n++){
			struct player* player = team->players[n];

			query = "INSERT INTO players ( \
					match_id, \
					side, \
					name, \
					splatnet_player_id, \
					is_submitter, \
					is_disconnect, \
					sex, \
					species, \
					weapon_id, \
					points, \
					level, \
					kill_count, \
					assist_count, \
					death_count, \
					special_count \
				) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15);";

			char* weapon_id = inttostr(player->weapon_id);
			char* points = inttostr(player->points);
			char* level = inttostr(player->level);
			char* kill_count = inttostr(player->kill_count);
			char* assist_count = inttostr(player->assist_count);
			char* death_count = inttostr(player->death_count);
			char* special_count = inttostr(player->special_count);

			if(!kore_pgsql_query_params(&pgsql, query, 0, 15,
				KORE_PGSQL_PARAM_TEXT(match_id),
				KORE_PGSQL_PARAM_TEXT(side),
				KORE_PGSQL_PARAM_TEXT(player->name),
				KORE_PGSQL_PARAM_TEXT(player->splatnet_player_id),
				KORE_PGSQL_PARAM_TEXT(player->is_submitter ? "true" : "false"),
				KORE_PGSQL_PARAM_TEXT(player->is_disconnect ? "true" : "false"),
				KORE_PGSQL_PARAM_TEXT(sextostr(player->sex)),
				KORE_PGSQL_PARAM_TEXT(speciestostr(player->species)),
				KORE_PGSQL_PARAM_TEXT(weapon_id),
				KORE_PGSQL_PARAM_TEXT(points),
				KORE_PGSQL_PARAM_TEXT(level),
				KORE_PGSQL_PARAM_TEXT(kill_count),
				KORE_PGSQL_PARAM_TEXT(assist_count),
				KORE_PGSQL_PARAM_TEXT(death_count),
				KORE_PGSQL_PARAM_TEXT(special_count)
			)){
				kore_pgsql_logerror(&pgsql);
				kore_pgsql_cleanup(&pgsql);
				kore_massfree(7, weapon_id, points, level, kill_count, assist_count, death_count, special_count);
				return KETTLE_SERVER_ERROR;
			}

			kore_massfree(7, weapon_id, points, level, kill_count, assist_count, death_count, special_count);
		}
		team = &match.bravo;
		side = "bravo";
	}

	kore_pgsql_cleanup(&pgsql);

	return KETTLE_SUCCESS;
}
