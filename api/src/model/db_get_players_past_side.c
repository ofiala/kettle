#include <kore/kore.h>
#include <kore/pgsql.h>
#include <kettle/structures.h>
#include <kettle/error.h>

/*!
 * @brief Finds the side that the given player played for previously. The result should not be freed.
 * @param The player whose side is to be found
 * @param out Where the side should be copied into
 * @return KETTLE_SUCCESS on success, KETTLE_CLIENT_ERROR on no past matches with the given player, and KETTLE_SERVER_ERROR on error
 */
int db_get_players_past_side(struct player player, enum side* out){
	if(out == NULL){
		kore_log(LOG_ERR, "db_get_players_past_side: expected valid out pointer, got NULL");
		return KETTLE_SERVER_ERROR;
	}

	struct kore_pgsql pgsql;

	kore_pgsql_init(&pgsql);

	if(!kore_pgsql_setup(&pgsql, "db", KORE_PGSQL_SYNC)){
		kore_pgsql_logerror(&pgsql);
		goto error;
	}

	if(!kore_pgsql_query_params(&pgsql, "SELECT DISTINCT side FROM players WHERE splatnet_player_id = $1;", 0, 1, KORE_PGSQL_PARAM_TEXT(player.splatnet_player_id))){
		kore_pgsql_logerror(&pgsql);
		goto error;
	}

	short num_rows = kore_pgsql_ntuples(&pgsql);
	if(num_rows == 0){
		kore_pgsql_cleanup(&pgsql);
		return KETTLE_CLIENT_ERROR;
	}else if(num_rows != 1){
		kore_log(LOG_ERR, "db_get_players_past_side: conflicting data found in the database, manual fix might be needed");
		goto error;
	}

	char* sidestr = kore_pgsql_getvalue(&pgsql, 0, 0);
	if(strcmp(sidestr, "alpha") == 0){
		*out = alpha;
	}else if(strcmp(sidestr, "bravo") == 0){
		*out = bravo;
	}else{
		kore_log(LOG_CRIT, "db_get_players_past_side: the database is broken");
		error:
			kore_pgsql_cleanup(&pgsql);
			return KETTLE_SERVER_ERROR;
	}

	kore_pgsql_cleanup(&pgsql);
	return KETTLE_SUCCESS;
}
