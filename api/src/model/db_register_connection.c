#include <stdio.h>
#include <string.h>
#include <kore/kore.h>
#include <kore/pgsql.h>
#include <kettle/config.h>

char* strrep(const char* str, const char* substr, const char* rep);

void kore_massfree(int count, ...);

/*!
 * @brief register new pgsql connection
 * @return 1 on success, 0 on error
 */
int db_register_connection(void){
	int buflen = strlen("hostaddr='' dbname='' user='' password=''")+4*255+1;

	char* db_host = strrep(g_config.db_host, "\\", "\\\\");
	char* tmp = strrep(db_host, "\'", "\\\'");
	kore_free(db_host);
	db_host = tmp;

	char* db_name = strrep(g_config.db_name, "\\", "\\\\");
	tmp = strrep(db_name, "\'", "\\\'");
	kore_free(db_name);
	db_name = tmp;

	char* db_user = strrep(g_config.db_user, "\\", "\\\\");
	tmp = strrep(db_user, "\'", "\\\'");
	kore_free(db_user);
	db_user = tmp;

	char* db_password = strrep(g_config.db_password, "\\", "\\\\");
	tmp = strrep(db_password, "\'", "\\\'");
	kore_free(db_password);
	db_password = tmp;

	char buf[buflen];
	int outlen = snprintf(buf, buflen, "hostaddr='%s' dbname='%s' user='%s' password='%s'", db_host, db_name, db_user, db_password);
	kore_massfree(4, db_host, db_name, db_user, db_password);

	if(outlen > 0 && outlen < buflen){
		kore_pgsql_register("db", buf);
		return 1;
	}else{
		kore_log(LOG_CRIT, "db_register_connection: creating pgsql connection string using snprintf failed");
		return 0;
	}
}
