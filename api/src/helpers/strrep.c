#include <string.h>
#include <kore/kore.h>

/*!
 * @brief Replaces all occurences of the given substring in the given string with the given replacement. The result should be freed with kore_free.
 * @return The modified string
 */
char* strrep(const char* str, const char* substr, const char* rep){
	char* strdup = kore_strdup(str);

	for(size_t i = 0;i <= strlen(strdup) - (strlen(substr)-1);i++){
		if(strncmp(strdup+i, substr, strlen(substr)) == 0){
			if(strlen(substr) == strlen(rep)){
				memmove(strdup+i, rep, strlen(rep));
			}else if(strlen(substr) < strlen(rep)){
				strdup = kore_realloc(strdup, strlen(strdup)+1 - strlen(substr) + strlen(rep));

				memmove(strdup+i + strlen(rep), strdup+i + strlen(substr), strlen(strdup+i + strlen(substr))+1);
				memmove(strdup+i, rep, strlen(rep));
			}else{
				memmove(strdup+i, rep, strlen(rep));
				memmove(strdup+i + strlen(rep), strdup+i + strlen(substr), strlen(strdup+i + strlen(substr))+1);

				strdup = kore_realloc(strdup, strlen(strdup)+1);
			}

			i += strlen(rep)-1;
		}
	}

	return strdup;
}
