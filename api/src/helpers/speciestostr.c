#include <kettle/structures.h>

/*!
 * @brief Returns a string equivalent to the given enum species name. The result should not be freed.
 */
const char* speciestostr(enum species species){
	static const char* const inklingstr = "inkling";
	static const char* const octolingstr = "octoling";

	switch(species){
		case(inkling):
			return inklingstr;
		case(octoling):
			return octolingstr;
	}
}
