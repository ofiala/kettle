#include <kettle/structures.h>

/*!
 * @brief Returns a string equivalent to the given enum sex name. The result should not be freed.
 */
const char* sextostr(enum sex sex){
	static const char* const malestr = "male";
	static const char* const femalestr = "female";

	switch(sex){
		case(male):
			return malestr;
		case(female):
			return femalestr;
	}
}
