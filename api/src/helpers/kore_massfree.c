#include <stdarg.h>
#include <kore/kore.h>

/*!
 * @brief Frees multiple memory blocks dynamically allocated with kore at once.
 * @param count Number of arguments
 */
void kore_massfree(int count, ...){
	va_list argv;
	va_start(argv, count);

	for(int i = 0;i < count;i++){
		kore_free(va_arg(argv, void*));
	}

	va_end(argv);
}
