#include <kettle/structures.h>

/*!
 * @brief Returns a string equivalent to the given enum side name. The result should not be freed.
 */
const char* sidetostr(enum side side){
	static const char* const alphastr = "alpha";
	static const char* const bravostr = "bravo";

	switch(side){
		case(alpha):
			return alphastr;
		case(bravo):
			return bravostr;
	}
}
