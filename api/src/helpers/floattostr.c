#include <stdio.h>
#include <float.h>
#include <kore/kore.h>

/*!
 * @brief Converts the given float to string. The result should be freed with kore_free if successful.
 * @return String value of the number on success, NULL on error
 */
char* floattostr(long double num){
	//determine max long double length in this system's libc implementation
	const size_t ldlen = 3 + DBL_MANT_DIG - DBL_MIN_EXP;

	char* str = kore_malloc(ldlen+1); //+1 for the terminating NUL character
	size_t resultlen = snprintf(str, ldlen+1, "%Lf", num);
	if(resultlen > 0 && resultlen < ldlen+1){
		return str;
	}else{
		kore_free(str);
		return NULL;
	}
}
