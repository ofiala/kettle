#include <stdio.h>
#include <limits.h>
#include <kore/kore.h>

/*!
 * @brief Converts the given integer to string. The result should be freed with kore_free if successful.
 * @return String value of the number on success, NULL on error
 */
char* inttostr(long long num){
	static size_t lllen = 0;

	if(lllen == 0){
		long long limit = LLONG_MAX;

		while(limit > 0){
			limit /= 10;
			lllen++;
		}

		lllen++; //optional negative sign
	}

	char* str = kore_malloc(lllen+1); //+1 for the terminating NUL character
	size_t resultlen = snprintf(str, lllen+1, "%lld", num);
	if(resultlen > 0 && resultlen < lllen+1){
		return str;
	}else{
		kore_free(str);
		return NULL;
	}
}
