#include <stdlib.h>
#include <kore/kore.h>

int db_register_connection(void);

void kore_worker_configure(void){
	if(!db_register_connection()){
		kore_log(LOG_CRIT, "couldn't register connection to the database, exiting");
		exit(1);
	}
}
