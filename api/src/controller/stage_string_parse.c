#include <string.h>
#include <kettle/error.h>

/*!
 * @brief Returns a stage id for the given stage string.
 * @return stage id on success, KETTLE_SERVER_ERROR on error
 */
short stage_string_parse(const char* stage_str){
	short stage_id;

	if(strcmp(stage_str, "#0") == 0 || strcmp(stage_str, "battera") == 0)
		stage_id = 0;
	else if(strcmp(stage_str, "#1") == 0 || strcmp(stage_str, "fujitsubo") == 0)
		stage_id = 1;
	else if(strcmp(stage_str, "#2") == 0 || strcmp(stage_str, "gangaze") == 0)
		stage_id = 2;
	else if(strcmp(stage_str, "#3") == 0 || strcmp(stage_str, "chozame") == 0)
		stage_id = 3;
	else if(strcmp(stage_str, "#4") == 0 || strcmp(stage_str, "ama") == 0)
		stage_id = 4;
	else if(strcmp(stage_str, "#5") == 0 || strcmp(stage_str, "kombu") == 0)
		stage_id = 5;
	else if(strcmp(stage_str, "#6") == 0 || strcmp(stage_str, "manta") == 0)
		stage_id = 6;
	else if(strcmp(stage_str, "#7") == 0 || strcmp(stage_str, "hokke") == 0)
		stage_id = 7;
	else if(strcmp(stage_str, "#8") == 0 || strcmp(stage_str, "tachiuo") == 0)
		stage_id = 8;
	else if(strcmp(stage_str, "#9") == 0 || strcmp(stage_str, "engawa") == 0)
		stage_id = 9;
	else if(strcmp(stage_str, "#10") == 0 || strcmp(stage_str, "mozuku") == 0)
		stage_id = 10;
	else if(strcmp(stage_str, "#11") == 0 || strcmp(stage_str, "bbas") == 0)
		stage_id = 11;
	else if(strcmp(stage_str, "#12") == 0 || strcmp(stage_str, "devon") == 0)
		stage_id = 12;
	else if(strcmp(stage_str, "#13") == 0 || strcmp(stage_str, "zatou") == 0)
		stage_id = 13;
	else if(strcmp(stage_str, "#14") == 0 || strcmp(stage_str, "hakofugu") == 0)
		stage_id = 14;
	else if(strcmp(stage_str, "#15") == 0 || strcmp(stage_str, "arowana") == 0)
		stage_id = 15;
	else if(strcmp(stage_str, "#16") == 0 || strcmp(stage_str, "mongara") == 0)
		stage_id = 16;
	else if(strcmp(stage_str, "#17") == 0 || strcmp(stage_str, "shottsuru") == 0)
		stage_id = 17;
	else if(strcmp(stage_str, "#18") == 0 || strcmp(stage_str, "ajifry") == 0)
		stage_id = 18;
	else if(strcmp(stage_str, "#19") == 0 || strcmp(stage_str, "otoro") == 0)
		stage_id = 19;
	else if(strcmp(stage_str, "#20") == 0 || strcmp(stage_str, "sumeshi") == 0)
		stage_id = 20;
	else if(strcmp(stage_str, "#21") == 0 || strcmp(stage_str, "anchovy") == 0)
		stage_id = 21;
	else if(strcmp(stage_str, "#22") == 0 || strcmp(stage_str, "mutsugoro") == 0)
		stage_id = 22;
	else if(strcmp(stage_str, "#100") == 0 || strcmp(stage_str, "mystery_04") == 0)
		stage_id = 100;
	else if(strcmp(stage_str, "#101") == 0 || strcmp(stage_str, "mystery_01") == 0)
		stage_id = 101;
	else if(strcmp(stage_str, "#102") == 0 || strcmp(stage_str, "mystery_02") == 0)
		stage_id = 102;
	else if(strcmp(stage_str, "#103") == 0 || strcmp(stage_str, "mystery_03") == 0)
		stage_id = 103;
	else if(strcmp(stage_str, "#105") == 0 || strcmp(stage_str, "mystery_07") == 0)
		stage_id = 105;
	else if(strcmp(stage_str, "#106") == 0 || strcmp(stage_str, "mystery_06") == 0)
		stage_id = 106;
	else if(strcmp(stage_str, "#107") == 0 || strcmp(stage_str, "mystery_05") == 0)
		stage_id = 107;
	else if(strcmp(stage_str, "#108") == 0 || strcmp(stage_str, "mystery_09") == 0)
		stage_id = 108;
	else if(strcmp(stage_str, "#109") == 0 || strcmp(stage_str, "mystery_10") == 0)
		stage_id = 109;
	else if(strcmp(stage_str, "#110") == 0 || strcmp(stage_str, "mystery_08") == 0)
		stage_id = 110;
	else if(strcmp(stage_str, "#111") == 0 || strcmp(stage_str, "mystery_11") == 0)
		stage_id = 111;
	else if(strcmp(stage_str, "#112") == 0 || strcmp(stage_str, "mystery_13") == 0)
		stage_id = 112;
	else if(strcmp(stage_str, "#113") == 0 || strcmp(stage_str, "mystery_12") == 0)
		stage_id = 113;
	else if(strcmp(stage_str, "#114") == 0 || strcmp(stage_str, "mystery_14") == 0)
		stage_id = 114;
	else if(strcmp(stage_str, "#115") == 0 || strcmp(stage_str, "mystery_15") == 0)
		stage_id = 115;
	else if(strcmp(stage_str, "#116") == 0 || strcmp(stage_str, "mystery_16") == 0)
		stage_id = 116;
	else if(strcmp(stage_str, "#117") == 0 || strcmp(stage_str, "mystery_17") == 0)
		stage_id = 117;
	else if(strcmp(stage_str, "#118") == 0 || strcmp(stage_str, "mystery_18") == 0)
		stage_id = 118;
	else if(strcmp(stage_str, "#119") == 0 || strcmp(stage_str, "mystery_19") == 0)
		stage_id = 119;
	else if(strcmp(stage_str, "#120") == 0 || strcmp(stage_str, "mystery_20") == 0)
		stage_id = 120;
	else if(strcmp(stage_str, "#121") == 0 || strcmp(stage_str, "mystery_21") == 0)
		stage_id = 121;
	else if(strcmp(stage_str, "#122") == 0 || strcmp(stage_str, "mystery_22") == 0)
		stage_id = 122;
	else if(strcmp(stage_str, "#123") == 0 || strcmp(stage_str, "mystery_23") == 0)
		stage_id = 123;
	else if(strcmp(stage_str, "#124") == 0 || strcmp(stage_str, "mystery_24") == 0)
		stage_id = 124;
	else if(strcmp(stage_str, "#9999") == 0 || strcmp(stage_str, "mystery") == 0)
		stage_id = 9999;
	else
		return KETTLE_SERVER_ERROR;

	return stage_id;
}
