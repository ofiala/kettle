#include <string.h>
#include <kore/kore.h>
#include <kettle/structures.h>
#include <kettle/error.h>

int db_get_user(const char* api_key, struct user* out);

/*!
 * @brief Tries to find a user based on the provided authentication string. The result should not be freed.
 * @param authstr Authentication string as provided in the Authorization HTTP header
 * @param out Where the user struct is stored
 * @return KETTLE_SUCCESS if the user was found, KETTLE_CLIENT_ERROR if not found/invalid string, and KETTLE_SERVER_ERROR on error
 */
int get_user_from_auth_string(char* authstr, struct user* out){
	if(authstr == NULL){
		kore_log(LOG_ERR, "get_user_from_auth_string: expected valid auth string pointer, got NULL");
		return KETTLE_CLIENT_ERROR;
	}else if(out == NULL){
		kore_log(LOG_ERR, "get_user_from_auth_string: expected valid out pointer, got NULL");
		return KETTLE_SERVER_ERROR;
	}

	//kore_split_string adds a NULL pointer at the end of the array, so we need one member more
	char* auth_data[3];

	int substr_count = kore_split_string(authstr, " ", auth_data, 3);

	if(substr_count != 2)
		return KETTLE_CLIENT_ERROR;

	if(strcmp(auth_data[0], "Bearer") != 0)
		return KETTLE_CLIENT_ERROR;

	return db_get_user((const char*) auth_data[1], out);
}
