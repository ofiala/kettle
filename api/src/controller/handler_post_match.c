#include <string.h>
#include <kore/kore.h>
#include <kore/http.h>
#include <kettle/structures.h>
#include <kettle/error.h>

int get_user_from_auth_string(char* authstr, struct user* out);

int msgpack_extract_match_data(const char* payload, size_t payload_len, enum side context, struct match* out);

int match_validate_players(struct match* match);

int check_match_eligibility(struct match match);

int db_save_match_data(struct match match_data, struct user submitter);

/*!
 * @brief Handler for the POST /match endpoint
 */
int handler_post_match(struct http_request* req){
	int ret = 0;

	/* CHECK HEADERS */
	const char* auth;

	if(!http_request_header(req, "Authorization", &auth)){
		http_response_header(req, "WWW-Authenticate", "Bearer realm=\"kettle/user\"");
		http_response(req, 401, NULL, 0);
		return KORE_RESULT_OK;
	}

	const char* content_type;

	if(!http_request_header(req, "Content-Type", &content_type) || strcmp(content_type, "application/x-msgpack") != 0){
		http_response(req, 415, NULL, 0);
		return KORE_RESULT_OK;
	}

	/* CHECK AUTHENTICATION VALIDITY */
	char* auth_dup = kore_strdup(auth);

	struct user submitter;
	ret = get_user_from_auth_string(auth_dup, &submitter);

	kore_free(auth_dup);

	//TODO msgpack error reporting
	if(ret == KETTLE_CLIENT_ERROR){
		http_response_header(req, "WWW-Authenticate", "Bearer realm=\"kettle/user\",\nerror=\"invalid_auth\",\nerror_description=\"The provided authentication is not valid\"");
		http_response(req, 401, NULL, 0);
		return KORE_RESULT_OK;
	}else if(ret == KETTLE_SERVER_ERROR){
		http_response(req, 500, NULL, 0);
		return KORE_RESULT_OK;
	}

	/* PARSE MSGPACK */
	char buffer[req->http_body_length];

	if(http_body_read(req, buffer, sizeof(buffer)) == KORE_RESULT_ERROR){
		http_response(req, 500, NULL, 0);
		return KORE_RESULT_OK;
	}

	struct match submitted_match;
	ret = msgpack_extract_match_data((const char*) buffer, sizeof(buffer), submitter.side, &submitted_match);

	if(ret == KETTLE_CLIENT_ERROR){
		http_response(req, 400, NULL, 0);
		return KORE_RESULT_OK;
	}else if(ret == KETTLE_SERVER_ERROR){
		http_response(req, 500, NULL, 0);
		return KORE_RESULT_OK;
	}

	/* VALIDATE PLAYERS */
	ret = match_validate_players(&submitted_match);
	if(ret == KETTLE_SERVER_ERROR){
		http_response(req, 500, NULL, 0);
		return KORE_RESULT_OK;
	}

	/* CHECK MATCH ELIGIBILITY */
	if(!check_match_eligibility(submitted_match)){
		match_free_players(&submitted_match);
		http_response(req, 403, NULL, 0);
		return KORE_RESULT_OK;
	}

	/* UPLOAD TO DATABASE */
	ret = db_save_match_data(submitted_match, submitter);
	if(ret == KETTLE_CLIENT_ERROR){
		match_free_players(&submitted_match);
		http_response(req, 302, NULL, 0);
		return KORE_RESULT_OK;
	}else if(ret == KETTLE_SERVER_ERROR){
		match_free_players(&submitted_match);
		http_response(req, 500, NULL, 0);
		return KORE_RESULT_OK;
	}

	/* CLEANUP */
	match_free_players(&submitted_match);

	http_response(req, 201, NULL, 0);
	return KORE_RESULT_OK;
}
