#include <kore/kore.h>
#include <kettle/structures.h>
#include <kettle/error.h>
#include <kettle/config.h>

int db_get_players_past_side(struct player player, enum side* out);

/*!
 * @brief Validates all players in the given match struct and removes the ones that are invalid.
 * @return KETTLE_SUCCESS on success, KETTLE_SERVER_ERROR on error
 */
int match_validate_players(struct match* match){
	if(match == NULL){
		kore_log(LOG_ERR, "match_validate_players: expected valid match struct pointer, got NULL");
		return KETTLE_SERVER_ERROR;
	}

	struct team* current_team = &match->alpha;
	enum side current_side = alpha;

	for(int i = 0;i < 2;i++){
		for(int n = 0;n < 4;n++){
			if(current_team->players[n] == NULL){
				continue;
			}

			if(current_side == alpha && strncmp(current_team->players[n]->name, g_config.alpha_prefix, strlen(g_config.alpha_prefix)) != 0 ||
			   current_side == bravo && strncmp(current_team->players[n]->name, g_config.bravo_prefix, strlen(g_config.bravo_prefix)) != 0){
				goto remove_player;
			}

			enum side past_side;
			int ret = db_get_players_past_side(*current_team->players[n], &past_side);
			if(ret == KETTLE_SERVER_ERROR){
				return KETTLE_SERVER_ERROR;
			}else if(ret == KETTLE_CLIENT_ERROR){
				continue;
			}

			if(past_side != current_side){
				remove_player:
					kore_free(current_team->players[n]);
					for(int x = n;x < current_team->player_count;x++){
						current_team->players[x] = current_team->players[x+1];
					}
					current_team->players[current_team->player_count] = NULL;
					current_team->player_count--;
					continue;
			}
		}

		current_team = &match->bravo;
		current_side = bravo;
	}

	return KETTLE_SUCCESS;
}
