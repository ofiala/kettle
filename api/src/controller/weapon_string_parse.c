#include <string.h>
#include <kettle/error.h>

/*!
 * @brief Returns a weapon id for the given weapon string.
 * @return weapon id on success, KETTLE_SERVER_ERROR on error
 */
short weapon_string_parse(const char* weapon_str){
	short weapon_id;

	if(strcmp(weapon_str, "#0") == 0 || strcmp(weapon_str, "bold") == 0)
		weapon_id = 0;
	else if(strcmp(weapon_str, "#1") == 0 || strcmp(weapon_str, "bold_neo") == 0)
		weapon_id = 1;
	else if(strcmp(weapon_str, "#2") == 0 || strcmp(weapon_str, "bold_7") == 0)
		weapon_id = 2;
	else if(strcmp(weapon_str, "#10") == 0 || strcmp(weapon_str, "wakaba") == 0)
		weapon_id = 10;
	else if(strcmp(weapon_str, "#11") == 0 || strcmp(weapon_str, "momiji") == 0)
		weapon_id = 11;
	else if(strcmp(weapon_str, "#12") == 0 || strcmp(weapon_str, "ochiba") == 0)
		weapon_id = 12;
	else if(strcmp(weapon_str, "#20") == 0 || strcmp(weapon_str, "sharp") == 0)
		weapon_id = 20;
	else if(strcmp(weapon_str, "#21") == 0 || strcmp(weapon_str, "sharp_neo") == 0)
		weapon_id = 21;
	else if(strcmp(weapon_str, "#30") == 0 || strcmp(weapon_str, "promodeler_mg") == 0)
		weapon_id = 30;
	else if(strcmp(weapon_str, "#31") == 0 || strcmp(weapon_str, "promodeler_rg") == 0)
		weapon_id = 31;
	else if(strcmp(weapon_str, "#32") == 0 || strcmp(weapon_str, "promodeler_pg") == 0)
		weapon_id = 32;
	else if(strcmp(weapon_str, "#40") == 0 || strcmp(weapon_str, "sshooter") == 0)
		weapon_id = 40;
	else if(strcmp(weapon_str, "#41") == 0 || strcmp(weapon_str, "sshooter_collabo") == 0)
		weapon_id = 41;
	else if(strcmp(weapon_str, "#42") == 0 || strcmp(weapon_str, "sshooter_becchu") == 0)
		weapon_id = 42;
	else if(strcmp(weapon_str, "#45") == 0 || strcmp(weapon_str, "heroshooter_replica") == 0)
		weapon_id = 45;
	else if(strcmp(weapon_str, "#46") == 0 || strcmp(weapon_str, "octoshooter_replica") == 0)
		weapon_id = 46;
	else if(strcmp(weapon_str, "#50") == 0 || strcmp(weapon_str, "52gal") == 0)
		weapon_id = 50;
	else if(strcmp(weapon_str, "#51") == 0 || strcmp(weapon_str, "52gal_deco") == 0)
		weapon_id = 51;
	else if(strcmp(weapon_str, "#52") == 0 || strcmp(weapon_str, "52gal_becchu") == 0)
		weapon_id = 52;
	else if(strcmp(weapon_str, "#60") == 0 || strcmp(weapon_str, "nzap85") == 0)
		weapon_id = 60;
	else if(strcmp(weapon_str, "#61") == 0 || strcmp(weapon_str, "nzap89") == 0)
		weapon_id = 61;
	else if(strcmp(weapon_str, "#62") == 0 || strcmp(weapon_str, "nzap83") == 0)
		weapon_id = 62;
	else if(strcmp(weapon_str, "#70") == 0 || strcmp(weapon_str, "prime") == 0)
		weapon_id = 70;
	else if(strcmp(weapon_str, "#71") == 0 || strcmp(weapon_str, "prime_collabo") == 0)
		weapon_id = 71;
	else if(strcmp(weapon_str, "#72") == 0 || strcmp(weapon_str, "prime_becchu") == 0)
		weapon_id = 72;
	else if(strcmp(weapon_str, "#80") == 0 || strcmp(weapon_str, "96gal") == 0)
		weapon_id = 80;
	else if(strcmp(weapon_str, "#81") == 0 || strcmp(weapon_str, "96gal_deco") == 0)
		weapon_id = 81;
	else if(strcmp(weapon_str, "#90") == 0 || strcmp(weapon_str, "jetsweeper") == 0)
		weapon_id = 90;
	else if(strcmp(weapon_str, "#91") == 0 || strcmp(weapon_str, "jetsweeper_custom") == 0)
		weapon_id = 91;
	else if(strcmp(weapon_str, "#200") == 0 || strcmp(weapon_str, "nova") == 0)
		weapon_id = 200;
	else if(strcmp(weapon_str, "#201") == 0 || strcmp(weapon_str, "nova_neo") == 0)
		weapon_id = 201;
	else if(strcmp(weapon_str, "#202") == 0 || strcmp(weapon_str, "nova_becchu") == 0)
		weapon_id = 202;
	else if(strcmp(weapon_str, "#210") == 0 || strcmp(weapon_str, "hotblaster") == 0)
		weapon_id = 210;
	else if(strcmp(weapon_str, "#211") == 0 || strcmp(weapon_str, "hotblaster_custom") == 0)
		weapon_id = 211;
	else if(strcmp(weapon_str, "#215") == 0 || strcmp(weapon_str, "heroblaster_replica") == 0)
		weapon_id = 215;
	else if(strcmp(weapon_str, "#220") == 0 || strcmp(weapon_str, "longblaster") == 0)
		weapon_id = 220;
	else if(strcmp(weapon_str, "#221") == 0 || strcmp(weapon_str, "longblaster_custom") == 0)
		weapon_id = 221;
	else if(strcmp(weapon_str, "#220") == 0 || strcmp(weapon_str, "longblaster_necro") == 0)
		weapon_id = 222;
	else if(strcmp(weapon_str, "#230") == 0 || strcmp(weapon_str, "clashblaster") == 0)
		weapon_id = 230;
	else if(strcmp(weapon_str, "#231") == 0 || strcmp(weapon_str, "clashblaster_neo") == 0)
		weapon_id = 231;
	else if(strcmp(weapon_str, "#240") == 0 || strcmp(weapon_str, "rapid") == 0)
		weapon_id = 240;
	else if(strcmp(weapon_str, "#241") == 0 || strcmp(weapon_str, "rapid_deco") == 0)
		weapon_id = 241;
	else if(strcmp(weapon_str, "#242") == 0 || strcmp(weapon_str, "rapid_becchu") == 0)
		weapon_id = 242;
	else if(strcmp(weapon_str, "#250") == 0 || strcmp(weapon_str, "rapid_elite") == 0)
		weapon_id = 250;
	else if(strcmp(weapon_str, "#251") == 0 || strcmp(weapon_str, "rapid_elite_deco") == 0)
		weapon_id = 251;
	else if(strcmp(weapon_str, "#300") == 0 || strcmp(weapon_str, "l3reelgun") == 0)
		weapon_id = 300;
	else if(strcmp(weapon_str, "#301") == 0 || strcmp(weapon_str, "l3reelgun_d") == 0)
		weapon_id = 301;
	else if(strcmp(weapon_str, "#302") == 0 || strcmp(weapon_str, "l3reelgun_becchu") == 0)
		weapon_id = 302;
	else if(strcmp(weapon_str, "#310") == 0 || strcmp(weapon_str, "h3reelgun") == 0)
		weapon_id = 310;
	else if(strcmp(weapon_str, "#311") == 0 || strcmp(weapon_str, "h3reelgun_d") == 0)
		weapon_id = 311;
	else if(strcmp(weapon_str, "#312") == 0 || strcmp(weapon_str, "h3reelgun_cherry") == 0)
		weapon_id = 312;
	else if(strcmp(weapon_str, "#400") == 0 || strcmp(weapon_str, "bottlegeyser") == 0)
		weapon_id = 400;
	else if(strcmp(weapon_str, "#401") == 0 || strcmp(weapon_str, "bottlegeyser_foil") == 0)
		weapon_id = 401;
	else if(strcmp(weapon_str, "#1000") == 0 || strcmp(weapon_str, "carbon") == 0)
		weapon_id = 1000;
	else if(strcmp(weapon_str, "#1001") == 0 || strcmp(weapon_str, "carbon_deco") == 0)
		weapon_id = 1001;
	else if(strcmp(weapon_str, "#1010") == 0 || strcmp(weapon_str, "splatroller") == 0)
		weapon_id = 1010;
	else if(strcmp(weapon_str, "#1011") == 0 || strcmp(weapon_str, "splatroller_collabo") == 0)
		weapon_id = 1011;
	else if(strcmp(weapon_str, "#1012") == 0 || strcmp(weapon_str, "splatroller_becchu") == 0)
		weapon_id = 1012;
	else if(strcmp(weapon_str, "#1015") == 0 || strcmp(weapon_str, "heroroller_replica") == 0)
		weapon_id = 1015;
	else if(strcmp(weapon_str, "#1020") == 0 || strcmp(weapon_str, "dynamo") == 0)
		weapon_id = 1020;
	else if(strcmp(weapon_str, "#1021") == 0 || strcmp(weapon_str, "dynamo_tesla") == 0)
		weapon_id = 1021;
	else if(strcmp(weapon_str, "#1022") == 0 || strcmp(weapon_str, "dynamo_becchu") == 0)
		weapon_id = 1022;
	else if(strcmp(weapon_str, "#1030") == 0 || strcmp(weapon_str, "variableroller") == 0)
		weapon_id = 1030;
	else if(strcmp(weapon_str, "#1031") == 0 || strcmp(weapon_str, "variableroller_foil") == 0)
		weapon_id = 1031;
	else if(strcmp(weapon_str, "#1100") == 0 || strcmp(weapon_str, "pablo") == 0)
		weapon_id = 1100;
	else if(strcmp(weapon_str, "#1101") == 0 || strcmp(weapon_str, "pablo_hue") == 0)
		weapon_id = 1101;
	else if(strcmp(weapon_str, "#1102") == 0 || strcmp(weapon_str, "pablo_permanent") == 0)
		weapon_id = 1102;
	else if(strcmp(weapon_str, "#1110") == 0 || strcmp(weapon_str, "hokusai") == 0)
		weapon_id = 1110;
	else if(strcmp(weapon_str, "#1111") == 0 || strcmp(weapon_str, "hokusai_hue") == 0)
		weapon_id = 1111;
	else if(strcmp(weapon_str, "#1112") == 0 || strcmp(weapon_str, "hokusai_becchu") == 0)
		weapon_id = 1112;
	else if(strcmp(weapon_str, "#1115") == 0 || strcmp(weapon_str, "herobrush_replica") == 0)
		weapon_id = 1115;
	else if(strcmp(weapon_str, "#2000") == 0 || strcmp(weapon_str, "squiclean_a") == 0)
		weapon_id = 2000;
	else if(strcmp(weapon_str, "#2001") == 0 || strcmp(weapon_str, "squiclean_b") == 0)
		weapon_id = 2001;
	else if(strcmp(weapon_str, "#2002") == 0 || strcmp(weapon_str, "squiclean_g") == 0)
		weapon_id = 2002;
	else if(strcmp(weapon_str, "#2010") == 0 || strcmp(weapon_str, "splatcharger") == 0)
		weapon_id = 2010;
	else if(strcmp(weapon_str, "#2011") == 0 || strcmp(weapon_str, "splatcharger_collabo") == 0)
		weapon_id = 2011;
	else if(strcmp(weapon_str, "#2012") == 0 || strcmp(weapon_str, "splatcharger_becchu") == 0)
		weapon_id = 2012;
	else if(strcmp(weapon_str, "#2015") == 0 || strcmp(weapon_str, "herocharger_replica") == 0)
		weapon_id = 2015;
	else if(strcmp(weapon_str, "#2020") == 0 || strcmp(weapon_str, "splatscope") == 0)
		weapon_id = 2020;
	else if(strcmp(weapon_str, "#2021") == 0 || strcmp(weapon_str, "splatscope_collabo") == 0)
		weapon_id = 2021;
	else if(strcmp(weapon_str, "#2022") == 0 || strcmp(weapon_str, "splatscope_becchu") == 0)
		weapon_id = 2022;
	else if(strcmp(weapon_str, "#2030") == 0 || strcmp(weapon_str, "liter4k") == 0)
		weapon_id = 2030;
	else if(strcmp(weapon_str, "#2031") == 0 || strcmp(weapon_str, "liter4k_custom") == 0)
		weapon_id = 2031;
	else if(strcmp(weapon_str, "#2040") == 0 || strcmp(weapon_str, "liter4k_scope") == 0)
		weapon_id = 2040;
	else if(strcmp(weapon_str, "#2041") == 0 || strcmp(weapon_str, "liter4k_scope_custom") == 0)
		weapon_id = 2041;
	else if(strcmp(weapon_str, "#2050") == 0 || strcmp(weapon_str, "bamboo14mk1") == 0)
		weapon_id = 2050;
	else if(strcmp(weapon_str, "#2051") == 0 || strcmp(weapon_str, "bamboo14mk2") == 0)
		weapon_id = 2051;
	else if(strcmp(weapon_str, "#2052") == 0 || strcmp(weapon_str, "bamboo14mk3") == 0)
		weapon_id = 2052;
	else if(strcmp(weapon_str, "#2060") == 0 || strcmp(weapon_str, "soytuber") == 0)
		weapon_id = 2060;
	else if(strcmp(weapon_str, "#2061") == 0 || strcmp(weapon_str, "soytuber_custom") == 0)
		weapon_id = 2061;
	else if(strcmp(weapon_str, "#3000") == 0 || strcmp(weapon_str, "bucketslosher") == 0)
		weapon_id = 3000;
	else if(strcmp(weapon_str, "#3001") == 0 || strcmp(weapon_str, "bucketslosher_deco") == 0)
		weapon_id = 3001;
	else if(strcmp(weapon_str, "#3002") == 0 || strcmp(weapon_str, "bucketslosher_soda") == 0)
		weapon_id = 3002;
	else if(strcmp(weapon_str, "#3005") == 0 || strcmp(weapon_str, "heroslosher_replica") == 0)
		weapon_id = 3005;
	else if(strcmp(weapon_str, "#3010") == 0 || strcmp(weapon_str, "hissen") == 0)
		weapon_id = 3010;
	else if(strcmp(weapon_str, "#3011") == 0 || strcmp(weapon_str, "hissen_hue") == 0)
		weapon_id = 3011;
	else if(strcmp(weapon_str, "#3020") == 0 || strcmp(weapon_str, "screwslosher") == 0)
		weapon_id = 3020;
	else if(strcmp(weapon_str, "#3021") == 0 || strcmp(weapon_str, "screwslosher_neo") == 0)
		weapon_id = 3021;
	else if(strcmp(weapon_str, "#3022") == 0 || strcmp(weapon_str, "screwslosher_becchu") == 0)
		weapon_id = 3022;
	else if(strcmp(weapon_str, "#3030") == 0 || strcmp(weapon_str, "furo") == 0)
		weapon_id = 3030;
	else if(strcmp(weapon_str, "#3031") == 0 || strcmp(weapon_str, "furo_deco") == 0)
		weapon_id = 3031;
	else if(strcmp(weapon_str, "#3040") == 0 || strcmp(weapon_str, "explosher") == 0)
		weapon_id = 3040;
	else if(strcmp(weapon_str, "#3041") == 0 || strcmp(weapon_str, "explosher_custom") == 0)
		weapon_id = 3041;
	else if(strcmp(weapon_str, "#4000") == 0 || strcmp(weapon_str, "splatspinner") == 0)
		weapon_id = 4000;
	else if(strcmp(weapon_str, "#4001") == 0 || strcmp(weapon_str, "splatspinner_collabo") == 0)
		weapon_id = 4001;
	else if(strcmp(weapon_str, "#4002") == 0 || strcmp(weapon_str, "splatspinner_becchu") == 0)
		weapon_id = 4002;
	else if(strcmp(weapon_str, "#4010") == 0 || strcmp(weapon_str, "barrelspinner") == 0)
		weapon_id = 4010;
	else if(strcmp(weapon_str, "#4011") == 0 || strcmp(weapon_str, "barrelspinner_deco") == 0)
		weapon_id = 4011;
	else if(strcmp(weapon_str, "#4012") == 0 || strcmp(weapon_str, "barrelspinner_remix") == 0)
		weapon_id = 4012;
	else if(strcmp(weapon_str, "#4015") == 0 || strcmp(weapon_str, "herospinner_replica") == 0)
		weapon_id = 4015;
	else if(strcmp(weapon_str, "#4020") == 0 || strcmp(weapon_str, "hydra") == 0)
		weapon_id = 4020;
	else if(strcmp(weapon_str, "#4021") == 0 || strcmp(weapon_str, "hydra_custom") == 0)
		weapon_id = 4021;
	else if(strcmp(weapon_str, "#4030") == 0 || strcmp(weapon_str, "kugelschreiber") == 0)
		weapon_id = 4030;
	else if(strcmp(weapon_str, "#4031") == 0 || strcmp(weapon_str, "kugelschreiber_hue") == 0)
		weapon_id = 4031;
	else if(strcmp(weapon_str, "#4040") == 0 || strcmp(weapon_str, "nautilus47") == 0)
		weapon_id = 4040;
	else if(strcmp(weapon_str, "#4041") == 0 || strcmp(weapon_str, "nautilus79") == 0)
		weapon_id = 4041;
	else if(strcmp(weapon_str, "#5000") == 0 || strcmp(weapon_str, "sputtery") == 0)
		weapon_id = 5000;
	else if(strcmp(weapon_str, "#5001") == 0 || strcmp(weapon_str, "sputtery_hue") == 0)
		weapon_id = 5001;
	else if(strcmp(weapon_str, "#5002") == 0 || strcmp(weapon_str, "sputtery_clear") == 0)
		weapon_id = 5002;
	else if(strcmp(weapon_str, "#5010") == 0 || strcmp(weapon_str, "maneuver") == 0)
		weapon_id = 5010;
	else if(strcmp(weapon_str, "#5011") == 0 || strcmp(weapon_str, "maneuver_collabo") == 0)
		weapon_id = 5011;
	else if(strcmp(weapon_str, "#5012") == 0 || strcmp(weapon_str, "maneuver_becchu") == 0)
		weapon_id = 5012;
	else if(strcmp(weapon_str, "#5015") == 0 || strcmp(weapon_str, "heromaneuver_replica") == 0)
		weapon_id = 5015;
	else if(strcmp(weapon_str, "#5020") == 0 || strcmp(weapon_str, "kelvin525") == 0)
		weapon_id = 5020;
	else if(strcmp(weapon_str, "#5021") == 0 || strcmp(weapon_str, "kelvin525_deco") == 0)
		weapon_id = 5021;
	else if(strcmp(weapon_str, "#5022") == 0 || strcmp(weapon_str, "kelvin525_becchu") == 0)
		weapon_id = 5022;
	else if(strcmp(weapon_str, "#5030") == 0 || strcmp(weapon_str, "dualsweeper") == 0)
		weapon_id = 5030;
	else if(strcmp(weapon_str, "#5031") == 0 || strcmp(weapon_str, "dualsweeper_custom") == 0)
		weapon_id = 5031;
	else if(strcmp(weapon_str, "#5040") == 0 || strcmp(weapon_str, "quadhopper_black") == 0)
		weapon_id = 5040;
	else if(strcmp(weapon_str, "#5041") == 0 || strcmp(weapon_str, "quadhopper_white") == 0)
		weapon_id = 5041;
	else if(strcmp(weapon_str, "#6000") == 0 || strcmp(weapon_str, "parashelter") == 0)
		weapon_id = 6000;
	else if(strcmp(weapon_str, "#6001") == 0 || strcmp(weapon_str, "parashelter_sorella") == 0)
		weapon_id = 6001;
	else if(strcmp(weapon_str, "#6005") == 0 || strcmp(weapon_str, "heroshelter_replica") == 0)
		weapon_id = 6005;
	else if(strcmp(weapon_str, "#6010") == 0 || strcmp(weapon_str, "campingshelter") == 0)
		weapon_id = 6010;
	else if(strcmp(weapon_str, "#6011") == 0 || strcmp(weapon_str, "campingshelter_sorella") == 0)
		weapon_id = 6011;
	else if(strcmp(weapon_str, "#6012") == 0 || strcmp(weapon_str, "campingshelter_camo") == 0)
		weapon_id = 6012;
	else if(strcmp(weapon_str, "#6020") == 0 || strcmp(weapon_str, "spygadget") == 0)
		weapon_id = 6020;
	else if(strcmp(weapon_str, "#6021") == 0 || strcmp(weapon_str, "spygadget_sorella") == 0)
		weapon_id = 6021;
	else if(strcmp(weapon_str, "#6022") == 0 || strcmp(weapon_str, "spygadget_becchu") == 0)
		weapon_id = 6022;
	else
		return KETTLE_SERVER_ERROR;

	return weapon_id;
}
