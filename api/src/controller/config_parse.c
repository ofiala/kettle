#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <wchar.h>
#include <locale.h>
#include <kore/kore.h>
#include <kettle/config.h>

//256 + length of the longest option name - NUL
#define MAX_LINE_LEN 267

struct config g_config;

/*!
 * @brief parse main config
 * @return 1 on success, 0 on error
 */
int config_parse(void){
	setlocale(LC_ALL, "C.UTF-8");

	char linebuf[MAX_LINE_LEN+2]; //NUL + newline
	bool option_set[7] = { false };
	char option_name[7][22] = {
		"db_host",
		"db_name",
		"db_user",
		"db_password",
		"alpha_prefix",
		"bravo_prefix",
		"min_team_player_count"
	};

	FILE* config = fopen("conf/main.conf", "r");
	if(config == NULL){
		kore_log(LOG_ERR, "error parsing config: failed to open file \"conf/main.conf\"");
		return 0;
	}

	for(int linenum = 1;fgets(linebuf, MAX_LINE_LEN+1, config) != NULL;linenum++){
		if(linebuf[0] == '#' || linebuf[0] == '\n')
			continue;

		char* newlineptr = strchr(linebuf, '\n');
		if(newlineptr == NULL){
			kore_log(LOG_ERR, "error parsing config: line %d too long, max 267 ascii characters allowed", linenum);
			return 0;
		}
		*newlineptr = '\0';

		char* optionptr = strchr(linebuf, '=');
		if(optionptr == NULL){
			kore_log(LOG_ERR, "error parsing config: unrecognized option \"%s\"", linebuf);
			return 0;
		}
		*optionptr = '\0';
		char* valueptr = optionptr+1;
		optionptr = linebuf;

		for(int i = 0;i < 8;i++){
			if(i == 7){
				kore_log(LOG_ERR, "error parsing config: unrecognized option \"%s\"", optionptr);
				return 0;
			}

			if(strcmp(optionptr, option_name[i]) == 0){
				if(option_set[i]){
					kore_log(LOG_ERR, "error parsing config: multiple definitions of option \"%s\"", option_name[i]);
					return 0;
				}

				switch(i){
					case 0:
						strcpy(g_config.db_host, valueptr);
						break;
					case 1:
						strcpy(g_config.db_name, valueptr);
						break;
					case 2:
						strcpy(g_config.db_user, valueptr);
						break;
					case 3:
						strcpy(g_config.db_password, valueptr);
						break;
					case 4:
					case 5:
						{
							int valuelen = 0;
							int lastlen = 0;
							char* tmp = valueptr;
							while((lastlen = mbrlen(tmp, 4, NULL)) > 0){
								tmp += lastlen;
								valuelen++;
							}

							if(valuelen > 10){
								kore_log(LOG_ERR, "error parsing config: value of option \"%s\" cannot be longer than 10 unicode characters", option_name[i]);
								return 0;
							}

							strcpy(i == 4 ? g_config.alpha_prefix : g_config.bravo_prefix, valueptr);
							break;
						}
					case 6:
						{
							int conversionerrno = 0;
							g_config.min_team_player_count = kore_strtonum((const char*) valueptr, 10, 0, 4, &conversionerrno);

							if(conversionerrno != KORE_RESULT_OK){
								kore_log(LOG_ERR, "error parsing config: value of option \"%s\" is invalid", option_name[i]);
								return 0;
							}
							break;
						}
				}

				option_set[i] = true;
				break;
			}
		}
	}
	fclose(config);

	for(int i = 0;i < 7;i++){
		if(!option_set[i]){
			kore_log(LOG_ERR, "error parsing config: required option \"%s\" missing", option_name[i]);
			return 0;
		}
	}

	return 1;
}
