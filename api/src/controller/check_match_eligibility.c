#include <kettle/structures.h>
#include <kettle/error.h>
#include <kettle/config.h>

/*!
 * @brief Checks whether the given match is eligible to be recorded.
 * @return KETTLE_SUCCESS on eligible, KETTLE_CLIENT_ERROR on not eligible
 */
int check_match_eligibility(struct match match){
	if(match.alpha.player_count - match.alpha.disconnect_count > match.bravo.player_count - match.bravo.disconnect_count && match.winning_side == alpha ||
	   match.alpha.player_count - match.alpha.disconnect_count < match.bravo.player_count - match.bravo.disconnect_count && match.winning_side == bravo ||
	   match.alpha.player_count < g_config.min_team_player_count ||
	   match.bravo.player_count < g_config.min_team_player_count){
		return KETTLE_CLIENT_ERROR;
	}

	return KETTLE_SUCCESS;
}
