#include <stdlib.h>
#include <kore/kore.h>
#include <kore/http.h>
#include <kettle/structures.h>
#include <kettle/error.h>

int get_user_from_auth_string(char* authstr, struct user* out);

int db_get_splatnet_match_ids_by_submitter(struct user submitter, long limit, long** out, long* out_count);

int msgpack_encode_splatnet_match_ids(const long* splatnet_match_ids, long count, char** out, size_t* out_len);

/*!
 * @brief Handler for the GET /my-matches/splatnet-match-ids endpoint
 */
int handler_get_my_splatnet_match_ids(struct http_request* req){
	int ret = 0;

	/* CHECK HEADERS */
	const char* auth;

	if(!http_request_header(req, "Authorization", &auth)){
		http_response_header(req, "WWW-Authenticate", "Bearer realm=\"kettle/user\"");
		http_response(req, 401, NULL, 0);
		return KORE_RESULT_OK;
	}

	/* CHECK AUTHENTICATION VALIDITY */
	char* auth_dup = kore_strdup(auth);

	struct user user;
	ret = get_user_from_auth_string(auth_dup, &user);

	kore_free(auth_dup);

	//TODO msgpack error reporting
	if(ret == KETTLE_CLIENT_ERROR){
		http_response_header(req, "WWW-Authenticate", "Bearer realm=\"kettle/user\",\nerror=\"invalid_auth\",\nerror_description=\"The provided authentication is not valid\"");
		http_response(req, 401, NULL, 0);
		return KORE_RESULT_OK;
	}else if(ret == KETTLE_SERVER_ERROR){
		http_response(req, 500, NULL, 0);
		return KORE_RESULT_OK;
	}

	/* GET SPLATNET MATCH IDS */
	long limit = 0;

	http_populate_qs(req);
	//kore should validate this to positive numbers only, no need to check
	http_argument_get_int32(req, "limit", &limit);

	long ids_found = 0;
	long* splatnet_match_ids = NULL;
	ret = db_get_splatnet_match_ids_by_submitter(user, limit, &splatnet_match_ids, &ids_found);
	if(ret == KETTLE_SERVER_ERROR){
		http_response(req, 500, NULL, 0);
		return KORE_RESULT_OK;
	}

	/* PREPARE MSGPACK PAYLOAD */
	char* payload = NULL;
	size_t payloadlen = 0;

	ret = msgpack_encode_splatnet_match_ids((const long*) splatnet_match_ids, ids_found, &payload, &payloadlen);
	kore_free(splatnet_match_ids);

	if(ret == KETTLE_SERVER_ERROR){
		http_response(req, 500, NULL, 0);
		return KORE_RESULT_OK;
	}

	/* SEND PAYLOAD */
	http_response_header(req, "Content-Type", "application/x-msgpack");
	http_response(req, 200, payload, payloadlen);
	free(payload);
	return KORE_RESULT_OK;
}
