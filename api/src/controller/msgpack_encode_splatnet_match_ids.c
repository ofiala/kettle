#include <kore/kore.h>
#include <mpack/mpack.h>
#include <kettle/error.h>

/*!
 * @brief Encode the given array of splatnet match ids into msgpack payload. The result should be freed with stdlib's free if successful.
 * @param splatnet_match_ids Array of splatnet match ids
 * @param count Number of members in the array
 * @param out Where the pointer to the payload should be stored; the payload string **is not** NUL terminated!
 * @param out_len Where the length of the payload should be copied into
 * @return KETTLE_SUCCESS on success, KETTLE_SERVER_ERROR on error
 */
int msgpack_encode_splatnet_match_ids(const long* splatnet_match_ids, long count, char** out, size_t* out_len){
	if(out == NULL || out_len == NULL){
		kore_log(LOG_ERR, "msgpack_encode_splatnet_match_ids: expected valid pointer, got NULL");
		return KETTLE_SERVER_ERROR;
	}

	mpack_writer_t writer;
	mpack_writer_init_growable(&writer, out, out_len);

	mpack_start_array(&writer, count);
	for(long i = 0;i < count;i++){
		mpack_write_i32(&writer, splatnet_match_ids[i]);
	}
	mpack_finish_array(&writer);

	mpack_error_t status = mpack_writer_destroy(&writer);
	if(status != mpack_ok){
		kore_log(LOG_ERR, "error encoding msgpack: %d", status);
		return KETTLE_SERVER_ERROR;
	}

	return KETTLE_SUCCESS;
}
