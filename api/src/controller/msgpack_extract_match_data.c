#include <stdbool.h>
#include <kore/kore.h>
#include <mpack/mpack.h>
#include <kettle/structures.h>
#include <kettle/error.h>

short stage_string_parse(const char* stage_str);

short weapon_string_parse(const char* weapon_str);

/*!
 * @brief Tries to extract match data from the given msgpack payload. The result should be freed with match_players_free if successful.
 * @param payload The msgpack payload containing match data
 * @param payload_len Length of the payload
 * @param context From which side's perspective was the match recorded
 * @param out Where the match struct is stored
 * @return KETTLE_SUCCESS on success, KETTLE_CLIENT_ERROR on bad data, and KETTLE_SERVER_ERROR on error
 */
int msgpack_extract_match_data(const char* payload, size_t payload_len, enum side context, struct match* out){
	if(payload == NULL){
		kore_log(LOG_ERR, "msgpack_extract_match_data: expected valid payload pointer, got NULL");
		return KETTLE_CLIENT_ERROR;
	}else if(out == NULL){
		kore_log(LOG_ERR, "msgpack_extract_match_data: expected valid out pointer, got NULL");
		return KETTLE_SERVER_ERROR;
	}

	struct match* match = out;
	match_init_players(match);

	mpack_tree_t tree;

	mpack_tree_init_data(&tree, payload, payload_len);
	mpack_tree_parse(&tree);

	mpack_node_t tree_root = mpack_tree_root(&tree);

	/******************
	 *** MATCH DATA ***
	 ******************/

	/* START OPTIONAL CHECKS */
	if(!mpack_node_is_missing(mpack_node_map_cstr_optional(tree_root, "lobby"))){
		const char* lobby_options[] = {"private"};
		mpack_node_enum(mpack_node_map_cstr(tree_root, "lobby"), lobby_options, 1);
	}

	if(!mpack_node_is_missing(mpack_node_map_cstr_optional(tree_root, "mode"))){
		const char* mode_options[] = {"private"};
		mpack_node_enum(mpack_node_map_cstr(tree_root, "mode"), mode_options, 1);
	}

	if(!mpack_node_is_missing(mpack_node_map_cstr_optional(tree_root, "rule"))){
		const char* rule_options[] = {"nawabari"};
		mpack_node_enum(mpack_node_map_cstr(tree_root, "rule"), rule_options, 1);
	}
	/* END OPTIONAL CHECKS */

	const char* result_options[] = {"lose", "win"};
	bool won = mpack_node_enum(mpack_node_map_cstr(tree_root, "result"), result_options, 2);
	match->winning_side = (enum side) context == bravo ? won : !won;

	char stage_str[11];
	mpack_node_copy_cstr(mpack_node_map_cstr(tree_root, "stage"), stage_str, 11);

	match->stage_id = stage_string_parse((const char*) stage_str);
	if(match->stage_id == KETTLE_SERVER_ERROR)
		mpack_tree_flag_error(&tree, mpack_error_data);


	match->splatnet_match_id = (long) mpack_node_i32(mpack_node_map_cstr(tree_root, "splatnet_number"));
	match->start_timestamp = (long) mpack_node_i32(mpack_node_map_cstr(tree_root, "start_at"));
	match->end_timestamp = (long) mpack_node_i32(mpack_node_map_cstr(tree_root, "end_at"));

	/*****************
	 *** TEAM DATA ***
	 *****************/

	struct team* good_guys;
	struct team* bad_guys;

	if(context == alpha){
		good_guys = &match->alpha;
		bad_guys = &match->bravo;
	}else{
		good_guys = &match->bravo;
		bad_guys = &match->alpha;
	}

	good_guys->player_count = 0;
	bad_guys->player_count = 0;

	good_guys->disconnect_count = 0;
	bad_guys->disconnect_count = 0;

	good_guys->painted_percent = mpack_node_float(mpack_node_map_cstr(tree_root, "my_team_percent"));
	bad_guys->painted_percent = mpack_node_float(mpack_node_map_cstr(tree_root, "his_team_percent"));

	/*******************
	 *** PLAYER DATA ***
	 *******************/

	mpack_node_t tree_players = mpack_node_map_cstr(tree_root, "players");
	size_t players_total = tree_players.data->len; //returns number of members of the 'tree_players' node

	for(size_t i = 0;i < players_total;i++){
		mpack_node_t tree_player = mpack_node_array_at(tree_players, i);

		const char* team_options[] = {"his", "my"};
		bool is_my_team = mpack_node_enum(mpack_node_map_cstr(tree_player, "team"), team_options, 2);

		struct team* team;

		if(is_my_team){
			good_guys->player_count++;
			team = good_guys;
		}else{
			bad_guys->player_count++;
			team = bad_guys;
		}

		struct player* player = kore_malloc(sizeof(struct player));

		const char* is_me_options[] = {"no", "yes"};
		player->is_submitter = mpack_node_enum(mpack_node_map_cstr(tree_player, "is_me"), is_me_options, 2);
		if(player->is_submitter)
			match->submitter = player;

		if(team == bad_guys && player->is_submitter){
			mpack_tree_flag_error(&tree, mpack_error_data);
			break;
		}

		const char* gender_options[] = {"boy", "girl"};
		player->sex = (enum sex) mpack_node_enum(mpack_node_map_cstr(tree_player, "gender"), gender_options, 2);

		const char* species_options[] = {"inkling", "octoling"};
		player->species = (enum species) mpack_node_enum(mpack_node_map_cstr(tree_player, "species"), species_options, 2);

		mpack_node_copy_cstr(mpack_node_map_cstr(tree_player, "splatnet_id"), player->splatnet_player_id, 17);
		mpack_node_copy_utf8_cstr(mpack_node_map_cstr(tree_player, "name"), player->name, 41);

		char weapon_str[23];
		mpack_node_copy_cstr(mpack_node_map_cstr(tree_player, "weapon"), weapon_str, 23);

		player->weapon_id = weapon_string_parse((const char*) weapon_str);
		if(player->weapon_id == KETTLE_SERVER_ERROR){
			mpack_tree_flag_error(&tree, mpack_error_data);
			break;
		}

		player->level = mpack_node_i16(mpack_node_map_cstr(tree_player, "level"));

		if(!mpack_node_is_missing(mpack_node_map_cstr_optional(tree_player, "star_rank"))){
			player->level += 99 * mpack_node_i16(mpack_node_map_cstr(tree_player, "star_rank"));
		}

		player->points = mpack_node_i16(mpack_node_map_cstr(tree_player, "point"));
		player->kill_count = mpack_node_i16(mpack_node_map_cstr(tree_player, "kill"));
		player->assist_count = mpack_node_i16(mpack_node_map_cstr(tree_player, "kill_or_assist")) - player->kill_count;
		player->death_count = mpack_node_i16(mpack_node_map_cstr(tree_player, "death"));
		player->special_count = mpack_node_i16(mpack_node_map_cstr(tree_player, "special"));

		if(team == good_guys && won && player->points == 1000 || team == good_guys && !won && player->points == 0 ||
		   team == bad_guys && won && player->points == 0 || team == bad_guys && !won && player->points == 1000){
			player->is_disconnect = true;
			team->disconnect_count++;
		}else{
			player->is_disconnect = false;
		}

		if(team->player_count <= 4){
			team->players[team->player_count-1] = player;
		}else{
			mpack_tree_flag_error(&tree, mpack_error_data);
			break;
		}
	}

	mpack_error_t status = mpack_tree_destroy(&tree);
	if(status == mpack_error_io || status == mpack_error_memory || status == mpack_error_bug){
		match_free_players(match);
		return KETTLE_SERVER_ERROR;
	}else if(status != mpack_ok){
		match_free_players(match);
		return KETTLE_CLIENT_ERROR;
	}

	return KETTLE_SUCCESS;
}
