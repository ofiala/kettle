#include <stdlib.h>
#include <kore/kore.h>

int config_parse(void);

void kore_parent_configure(int argc, char** argv){
	if(!config_parse()){
		kore_log(LOG_CRIT, "couldn't parse config, exiting");
		exit(1);
	}
}
