# Introduction

kettle features a pretty simple API that provides its clients the bare minimum to efficiently upload
matches. Since a similar, much more complex project focused on recording Splatoon matches in general
(as opposed to only custom Splatfest matches) already exists, kettle replicates some of its API
endpoints' features to make most stat.ink clients almost perfectly compatible with kettle by default.
Each file in this folder documents one of kettle's endpoints. All files include a "stat.ink
compatibility" section that identifies stat.ink's equivalent and describes important differences
between them.
