# POST /match

## Request headers

|Name         |Value                |
|-------------|---------------------|
|Authorization|Bearer \<API key\>   |
|Content-Type |application/x-msgpack|


## Request body

MessagePack payload containing the following data:

|Required?|Name              |Data type|Values                                       |Note                                                       |
|---------|------------------|---------|---------------------------------------------|-----------------------------------------------------------|
|no       |lobby             |string   |only `private` allowed                       |optional security measure for clients that upload all matches (as opposed to uploading only private turf matches) to make sure no ranked or public match slips through|
|no       |mode              |string   |only `private` allowed                       |(same as lobby)                                            |
|no       |rule              |string   |only `nawabari` allowed                      |(same as lobby)                                            |
|yes      |result            |string   |either `win` or `lose` (from submitter's POV)|                                                           |
|yes      |stage             |string   |`#<stage id>` or stat.ink stage name         |stage id supplied by SplatNet 2                            |
|yes      |splatnet_number   |int32    |SplatNet 2 match id (unique per player)      |match # supplied by SplatNet 2                             |
|yes      |start_at          |int32    |match start time as UNIX timestamp           |                                                           |
|yes      |end_at            |int32    |match end time as UNIX timestamp             |                                                           |
|yes      |my\_team\_percent |float    |submitter's team's inked percentage          |                                                           |
|yes      |his\_team\_percent|float    |enemy team's inked percentage                |                                                           |
|yes      |players           |array    |contains one member array for each player    |member arrays' contents described below                    |

Members of the players array do not have a name. Each member array should contain the following data:

|Required?|Name            |Data type|Values                                        |Note                            |
|---------|----------------|---------|----------------------------------------------|--------------------------------|
|yes      |team            |string   |`my` for allies, `his` for enemies            |                                |
|yes      |is_me           |string   |either `yes` or `no`                          |                                |
|yes      |gender          |string   |either `boy` or `girl`                        |                                |
|yes      |species         |string   |either `inkling` or `octoling`                |                                |
|yes      |splatnet_id     |string   |SplatNet 2 player id (universally unique)     |player id supplied by SplatNet 2|
|yes      |name            |string   |player's name, max 10 characters              |supports UTF-8                  |
|yes      |weapon          |string   |`#<weapon id>` or stat.ink weapon name        |weapon id supplied by SplatNet 2|
|yes      |level           |int16    |player's level                                |                                |
|no       |star_rank       |int16    |how many times was player's level reset       |                                |
|yes      |point           |int16    |how many points the player got incl. win bonus|                                |
|yes      |kill            |int16    |kill count                                    |                                |
|yes      |kill\_or\_assist|int16    |kill count incl. assists                      |                                |
|yes      |death           |int16    |death count                                   |                                |
|yes      |special         |int16    |special count                                 |                                |


## Possible response status codes

|Code|Meaning                                                    |
|----|-----------------------------------------------------------|
|201 |Match successfully uploaded                                |
|302 |Match with given SplatNet 2 id already exists for this user|
|400 |Invalid payload data                                       |
|401 |Bad auth                                                   |
|403 |Match not eligible for upload (mirrors etc.)               |
|413 |Payload too large                                          |
|415 |Bad declared content type                                  |
|500 |Internal server error                                      |

## stat.ink compatibility

This endpoint is similar to stat.ink's `POST /api/v2/battle`. The main differences between them are
JSON not being supported by kettle, stat.ink using less status codes, and no response body from
kettle in case of an error. The payload is entirely based on stat.ink's one to maintain compatibility
with existing stat.ink clients. Any extra data in the payload that is not specified above is safely ignored.
