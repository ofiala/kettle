# GET /my-matches/splatnet-match-ids

## Request headers

|Name         |Value             |
|-------------|------------------|
|Authorization|Bearer \<API key\>|


## Optional GET args

|Name |Values     |Meaning                          |
|-----|-----------|---------------------------------|
|limit|any integer|max number of match IDs to return|


## Possible response status codes

|Code|Meaning              |
|----|---------------------|
|200 |OK                   |
|401 |Bad auth             |
|500 |Internal server error|


## Response body

MessagePack payload containing an array of either all match IDs, or latest \<number\> of match IDs if
the optional limit argument is supplied. Match IDs are always ordered descendingly.


## stat.ink compatibility

This endpoint is similar to stat.ink's
`GET /api/v2/user-battle?order=splatnet\_desc&only=splatnet\_number` with argument `limit` being
similar to user-battle's argument `count`. The main difference is that it returns data as MessagePack
instead of JSON.
