# kettle: custom Splatfest results recording automation

kettle is an experimental program that provides API to which compatible clients can send custom
Splatfest matches' data. That data is then validated and matches are checked for fairness.
If the data is valid and match is fair, it gets uploaded to database. Once custom Splatfest is over,
results can then be easily calculated with few SQL commands. And since everything you see on your
in-game results screen is saved in the database, you are not limited to only Splatfest results! You
can get info on the most often used weapons, average kills, deaths, average difference in ink
coverage between the two teams, and more! Unfortunately, the software isn't 100% reliable yet, so if
you decide to use it for your custom Splatfest, make sure to have a backup option for recording
results such as the good old pen and paper.


## Requirements

### UNIX-like environment

Either a full UN\*X operating system, or a UN\*X environment is required. UN\*X operating systems
include MacOS, various Linux distributions, and \*BSDs. UN\*X environments include emulated Linux
environments such as MinGW for Windows. Instructions on how to set these up can be found online.


### Kore C framework

kettle builds upon the Kore C framework, which takes care of networking and makes dealing with some
other tasks much easier. Version 3.3.1 is currently supported, which can be downloaded from
[here](https://kore.io/releases/3.3.1). Installation instructions and requirements can be found
[here](https://docs.kore.io/3.3.1/install.html). Kore must be built with PostgreSQL support.


### PostgreSQL database

Either local or remote PostgreSQL database is required. Installation and setup instructions for your
platform can be found online.


## Setup

### Database

Run the SQL commands located in api/setup.sql. Make sure to create new user for every person that's
going to upload data to the server.


### Config

Modify the kettle config file (api/conf/main.conf) to match your configuration and preferences.
Although it is possible to run without team prefixes, they're strongly recommended as they provide
the much needed context that data from Nintendo lacks.


### Optional: Kore

If you require specific features such as SSL or special compilation flags, you might want to look
into tweaking some of Kore's settings. Kore's main config for this project can be found
at api/conf/api.conf, build config can be found at api/conf/build.conf. Available options for main
config are documented
[here](https://raw.githubusercontent.com/jorisvink/kore/1a64d202dea6caa6795e26bde022de6c2c89b78e/conf/kore.conf.example).

## Compilation

*Note: `cd` to api/ first.*

All Kore applications including kettle can be compiled by running `kodev build`.


## Running

*Note: `cd` to api/ first.*

kettle can be run in the same way any other Kore application can, using the `kore -c conf/api.conf`
command. Chrooting and running workers as a separate user is enabled by default, which means you need
to create a new user `kore`, and run the `kore` command as root every time so that it can chroot.
After chrooting, it runs the worker processes as the separate user `kore` that you created for this
purpose, so there is no need to worry about your security being compromised by a bug in kettle
as it's run by worker processes without superuser privileges. However, if you want to turn of either
chrooting or running as a separate user, just add the `-n` or `-r` flags, respectively. `kore` also
runs all processes in background by default, with errors and other messages being logged to syslog.
If you would prefer to run it in foreground instead, add the `-f` flag.


## Security

Due to its current design, kettle can be easily DoSed. This is caused by all SQL queries being
executed synchronously, not asynchronously, meaning the worker is unable to process other requests
while it waits for its SQL request to be completed. This is a design flaw I intend to fix
in the future. In the meantime, it is strongly recommended that you do not run kettle
on Internet-facing servers that you cannot afford having downtime. Also, since neither kettle
nor Kore wasn't audited by any third party, it might be a good idea to incorporate additional
security measures such as adding AppArmor profile. These recommendations do not apply when running
kettle on a server without public IP address inside a trusted LAN (desktops usually fall into this
category). You can also make kettle run only on localhost by replacing `0.0.0.0` with `127.0.0.1` in
api/conf/api.conf.


## Clients

The following clients currently support kettle:
- [splatnet2kettle](https://gitlab.com/ofiala/splatnet2kettle) (fork of stat.ink client
  splatnet2statink)
- other existing stat.ink clients can be converted to kettle clients by making minor modifications
  to their source code, see doc/


## Getting data from database

The whole point of kettle is that it allows you to easily upload custom Splatfest match data
to a database, where you can run SQL queries on it to get various types of data from it in a very
straight-forward way. Below are some examples of what can be done with such queries.

1. Get both teams' win count

```sql
SELECT
	(SELECT COUNT(*) FROM matches WHERE winning_side = 'alpha')
AS alpha_win_count,
	(SELECT COUNT(*) FROM matches WHERE winning_side = 'bravo')
AS bravo_win_count;
```

2. Get both teams' member count

```sql
SELECT
	(SELECT COUNT(DISTINCT splatnet_player_id) FROM players WHERE side = 'alpha')
AS alpha_player_count,
	(SELECT COUNT(DISTINCT splatnet_player_id) FROM players WHERE side = 'bravo')
AS bravo_player_count;
```

3. Get both teams' Clout (excl. synergy)

```sql
SELECT
	(SELECT SUM(points) FROM players WHERE side = 'alpha')
AS alpha_clout,
	(SELECT SUM(points) FROM players WHERE side = 'bravo')
AS bravo_clout;
```

4. Get top 10 most popular weapons

*Note: only weapon IDs are uploaded to the database, you can look up weapons' names
[here](https://stat.ink/api-info/weapon2).*

```sql
SELECT DISTINCT weapon_id, COUNT(*)
FROM players
GROUP BY weapon_id
ORDER BY COUNT(*) DESC
LIMIT 10;
```
